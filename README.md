# Shared Library Injection
Injects a shared library into a running process

## Read in shared library

```
call open("inject.o", 2)
$1 = 3 						# 3 = file descriptor
call mmap(0, 1488, 1 | 2 | 4, 1, 3, 0) 		# 3 is the fd from above
$2 = 1914929152	

cat /proc/PID/maps|grep injection.o
a = 7f770dff8000		# inject.o library added here
readelf -r process
b = 000000601010		# foo symbol relocation in executable file
readelf -S inject.o
c = 00000050		# .text offset section starts at in the inject.o

set * b = a + c
```

## Resolve function of injected library
```
readelf -r injection.o
d[x] = 0x0000000e		# function Offset

p & function x
e[x] = 0x733650		# function address

p * (a+c+d[x])
f[x] = -4			# function Addend

set * (a+c+d[x]) = e[x] - (a+c+d[x]) + f[x]
```

## Resolve rodata section
```
readelf -r injection.o
d[rodata] = 0x00000009	# rodata Offset

p * (a+c+d[rodata])
f[rodata] = 0			#rodata Addend

readelf -S injection.o
g = 0x000050		# .rodata Offset

set * (a+c+d[data]) = a + g + f[rodata]

gcc library.c -ggdb -Wall -fPIC -shared -o liblibrary.so
sudo cp liblibrary.so /lib
gcc -ggdb process.c -llibrary -llibrary -L./ -o process
gcc -Wall inject.c -c -o inject.o

call open("inject.o", 2)
$1 = 3 						# 3 = file descriptor
call mmap(0, 1488, 1 | 2 | 4, 1, 3, 0) 		# 3 is the fd from above
$2 = 1914929152					

cat /proc/PID/maps|grep inject.o
7f69bb728000-7f69bb729000 rwxs 00000000 fc:00 277461                     /home/akan/inject/inject.o

inject.o file was loaded into the executable process memory at address 0x7f69bb728000

readelf -r process
Relocation section '.rela.dyn' at offset 0x4b8 contains 1 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000600fe0  000300000006 R_X86_64_GLOB_DAT 0000000000000000 __gmon_start__ + 0

Relocation section '.rela.plt' at offset 0x4d0 contains 4 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000601000  000100000007 R_X86_64_JUMP_SLO 0000000000000000 puts + 0
000000601008  000200000007 R_X86_64_JUMP_SLO 0000000000000000 __libc_start_main + 0
000000601010  000400000007 R_X86_64_JUMP_SLO 0000000000000000 foo + 0
000000601018  000600000007 R_X86_64_JUMP_SLO 0000000000000000 sleep + 0

foo symbol relocation is located at the absolute (virtual) address (offset) 000000601010

readelf -s inject.o
Symbol table '.symtab' contains 11 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND 
     1: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS inject.c
     2: 0000000000000000     0 SECTION LOCAL  DEFAULT    1 
     3: 0000000000000000     0 SECTION LOCAL  DEFAULT    3 
     4: 0000000000000000     0 SECTION LOCAL  DEFAULT    4 
     5: 0000000000000000     0 SECTION LOCAL  DEFAULT    5 
     6: 0000000000000000     0 SECTION LOCAL  DEFAULT    7 
     7: 0000000000000000     0 SECTION LOCAL  DEFAULT    8 
     8: 0000000000000000     0 SECTION LOCAL  DEFAULT    6 
     9: 0000000000000000    16 FUNC    GLOBAL DEFAULT    1 inject
    10: 0000000000000000     0 NOTYPE  GLOBAL DEFAULT  UND puts
```

The function (symbol) inject is located at the offset 0 in the .text section in the injection.o object file

```
readelf -S inject.o

Section Headers:
  [Nr] Name              Type             Address           Offset	Size              EntSize          Flags  Link  Info  Align
  [ 0]                   NULL             0000000000000000  00000000	0000000000000000  0000000000000000           0     0     0
  [ 1] .text             PROGBITS         0000000000000000  00000040	0000000000000010  0000000000000000  AX       0     0     4
```

.text section starts at the offset 0x00000040 in the injection.o object file.

Confirm that 0x000000601010 points to foo

```
(gdb) p * 0x000000601010
$4 = 1910605308 # equal to 0x71e185fc
```

Address of inject() function is 0x7f69bb728000 + 0x00000040
```
set * 0x000000601010 = 0x7f69bb728000 + 0x00000040

readelf -r inject.o
Relocation section '.rela.text' at offset 0x588 contains 2 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000000005  00050000000a R_X86_64_32       0000000000000000 .rodata + 0
00000000000a  000a00000002 R_X86_64_PC32     0000000000000000 puts - 4

Relocation section '.rela.eh_frame' at offset 0x5b8 contains 1 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000000020  000200000002 R_X86_64_PC32     0000000000000000 .text + 0
```

Resolve relocations for .rodata & puts

Remap the addresses of these relocations within the executable process address space
